import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import VerificationForm from './VerificationForm';
import CreateCMSSignedForm from "./CreateCMSSignedForm";

class App extends Component {
    
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">CMS Verification Demo</h1>
        </header>
          <p className="App-intro">
              Create CMS Signed Form
          </p>
          <CreateCMSSignedForm />
        <p className="App-intro">
          Verification Form
        </p>
        <VerificationForm />
      </div>
    );
  }
}

export default App;
