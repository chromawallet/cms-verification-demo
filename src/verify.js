import * as asn1js from "asn1js";
import * as pvutils from "pvutils";
import * as Promise from "bluebird";
import * as isBase64 from "is-base64";
import * as pkijs from "pkijs";
import * as atob from "atob";

export default function verify(certificate, cms, signedText, verifyText) {

    // const pems = require('./pems')
    // const pvutils = require('pvutils')
    // require("babel-polyfill");
    // const Promise = require('bluebird')
    // const isBase64 = require('is-base64')

    let webcrypto = window.crypto || window.msCrypto;

    // const asn1js = require("asn1js");
    // const pkijs = require("pkijs");
    // const atob = require("atob");
    const Certificate = pkijs.Certificate;

    pkijs.setEngine("newEngine", webcrypto, new pkijs.CryptoEngine({
        name: "",
        crypto: webcrypto,
        subtle: webcrypto.subtle
    }))
// var dercertificate = base64ToArrayBuffer(pems.certificate)
// console.log(Array.prototype.map.call(new Uint8Array(dercertificate), x => ('00' + x.toString(16)).slice(-2)).join(''))
// console.log(dercertificate)
// var trustedCertificates = []
// parseCAbundle(dercertificate)

    const cmsSignedBuffer = base64ToArrayBuffer(cms);
    const certificateBuffer = stringToArrayBuffer(certificate);
    let trustedCertificates = [];
    console.log(certificateBuffer);
    parseCAbundle(certificateBuffer);
    console.log("begin certificates");
    console.log(trustedCertificates);
    console.log("end certificates");
    const asn1 = asn1js.fromBER(cmsSignedBuffer);
    const cmsContentSimpl = new pkijs.ContentInfo({schema: asn1.result});
    const cmsSignedSimpl = new pkijs.SignedData({schema: cmsContentSimpl.content});

    const dgstmap = {
        "1.3.14.3.2.26": "SHA-1",
        "2.16.840.1.101.3.4.2.1": "SHA-256",
        "2.16.840.1.101.3.4.2.2": "SHA-384",
        "2.16.840.1.101.3.4.2.3": "SHA-512"
    };

    for (let i = 0; i < cmsSignedSimpl.digestAlgorithms.length; i++) {
        let typeval = dgstmap[cmsSignedSimpl.digestAlgorithms[i].algorithmId];
        if (typeof typeval === "undefined")
            typeval = cmsSignedSimpl.digestAlgorithms[i].algorithmId;

        console.log(`    Type value is: ${typeval}`);

    }
    //endregion

    //region Put information about encapsulated content type
    const contypemap = {
        "1.3.6.1.4.1.311.2.1.4": "Authenticode signing information",
        "1.2.840.113549.1.7.1": "encapsulated content type is 'Data content'"
    };

    let eContentType = contypemap[cmsSignedSimpl.encapContentInfo.eContentType];
    if (typeof eContentType === "undefined")
        eContentType = cmsSignedSimpl.encapContentInfo.eContentType;

    console.log("econtent type is " + eContentType);
    //endregion

    //region Put information about included certificates
    const rdnmap = {
        "2.5.4.6": "C",
        "2.5.4.10": "O",
        "2.5.4.11": "OU",
        "2.5.4.3": "CN",
        "2.5.4.7": "L",
        "2.5.4.8": "S",
        "2.5.4.12": "T",
        "2.5.4.42": "GN",
        "2.5.4.43": "I",
        "2.5.4.4": "SN",
        "1.2.840.113549.1.9.1": "E-mail"
    };

    if ("certificates" in cmsSignedSimpl) {
        for (let j = 0; j < cmsSignedSimpl.certificates.length; j++) {
            // let ul = "<ul>";

            for (let i = 0; i < cmsSignedSimpl.certificates[j].issuer.typesAndValues.length; i++) {
                let typeval = rdnmap[cmsSignedSimpl.certificates[j].issuer.typesAndValues[i].type];
                if (typeof typeval === "undefined")
                    typeval = cmsSignedSimpl.certificates[j].issuer.typesAndValues[i].type;

                const subjval = cmsSignedSimpl.certificates[j].issuer.typesAndValues[i].value.valueBlock.value;
                console.log(`    Certificate found: ${typeval} ${subjval}`);

            }


            console.log(`Hex values: ${cmsSignedSimpl.certificates[j].serialNumber.valueBlock.valueHex}`);
        }

    }
    //endregion

    //region Put information about included CRLs
    if ("crls" in cmsSignedSimpl) {
        for (let j = 0; j < cmsSignedSimpl.crls.length; j++) {
            // let ul = "<ul>";

            for (let i = 0; i < cmsSignedSimpl.crls[j].issuer.typesAndValues.length; i++) {
                let typeval = rdnmap[cmsSignedSimpl.crls[j].issuer.typesAndValues[i].type];
                if (typeof typeval === "undefined")
                    typeval = cmsSignedSimpl.crls[j].issuer.typesAndValues[i].type;

                const subjval = cmsSignedSimpl.crls[j].issuer.typesAndValues[i].value.valueBlock.value;
                console.log(`Included CRLs: ${typeval}  ${subjval}`);

            }


        }

    }
    else {
        console.log("No certificate revocation lists found")
    }

    function stringToArrayBuffer(arg) {
        //valid data uri
        if (/^data/i.test(arg)) return decode(arg);

        //base64
        if (isBase64(arg)) arg = atob(arg);

        return str2ab(arg)
    }

    function str2ab(str) {
        var array = new Uint8Array(str.length);
        for (var i = 0; i < str.length; i++) {
            array[i] = str.charCodeAt(i);
        }
        return array.buffer
    }

    function decode(uri) {
        // strip newlines
        uri = uri.replace(/\r?\n/g, '');

        // split the URI up into the "metadata" and the "data" portions
        var firstComma = uri.indexOf(',');
        if (-1 === firstComma || firstComma <= 4) throw new TypeError('malformed data-URI');

        // remove the "data:" scheme and parse the metadata
        var meta = uri.substring(5, firstComma).split(';');

        var base64 = false;
        var charset = 'US-ASCII';
        for (var i = 0; i < meta.length; i++) {
            if ('base64' === meta[i]) {
                base64 = true;
            } else if (0 === meta[i].indexOf('charset=')) {
                charset = meta[i].substring(8);
            }
        }

        // get the encoded data portion and decode URI-encoded chars
        var data = unescape(uri.substring(firstComma + 1));

        if (base64) data = atob(data)

        var abuf = str2ab(data)

        abuf.type = meta[0] || 'text/plain'
        abuf.charset = charset

        return abuf
    }

    function base64ToArrayBuffer(base64) {
        var binary_string = atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }


//region Verify existing CMS_Signed
//*********************************************************************************
    function verifyCMSSignedInternal() {
        //region Initial check
        if (cmsSignedBuffer.byteLength === 0)
            return Promise.reject("Nothing to verify!");
        //endregion

        return Promise.resolve().then(() => {
            //region Decode existing CMS_Signed
            const asn1 = asn1js.fromBER(cmsSignedBuffer);
            const cmsContentSimpl = new pkijs.ContentInfo({schema: asn1.result});
            const cmsSignedSimpl = new pkijs.SignedData({schema: cmsContentSimpl.content});
            //endregion

            //region Verify CMS_Signed
            // const verificationParameters = {
            // 	signer: 0,
            // 	trustedCerts: trustedCertificates
            // };

            const verificationParameters = {
                signer: 0,
                trustedCerts: trustedCertificates,
                checkChain: true
            };


            return cmsSignedSimpl.verify(verificationParameters);
        });
        //endregion
    }

//*********************************************************************************
    function verifyCMSSigned(cmsSignedBuffer) {
        //region Initial check
        if (cmsSignedBuffer.byteLength === 0) {
            console.log("Nothing to verify!");
            return;
        }
        //endregion

        return verifyCMSSignedInternal(cmsSignedBuffer).then(
            result => `Verification result: ${result}`,
            error => `Error during verification: ${error}`
        );
    }


    function parseCAbundle(buffer) {
        //region Initial variables
        const base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

        const startChars = "-----BEGIN CERTIFICATE-----";
        const endChars = "-----END CERTIFICATE-----";
        const endLineChars = "\r\n";

        const view = new Uint8Array(buffer);

        let waitForStart = false;
        let middleStage = true;
        let waitForEnd = false;
        let waitForEndLine = false;
        let started = false;

        let certBodyEncoded = "";
        //endregion

        for (let i = 0; i < view.length; i++) {
            if (started === true) {
                if (base64Chars.indexOf(String.fromCharCode(view[i])) !== (-1)) {
                    certBodyEncoded = certBodyEncoded + String.fromCharCode(view[i]);
                }
                else {
                    if (String.fromCharCode(view[i]) === "-") {
                        //region Decoded trustedCertificates
                        const asn1 = asn1js.fromBER(pvutils.stringToArrayBuffer(atob(certBodyEncoded)));
                        try {
                            trustedCertificates.push(new Certificate({schema: asn1.result}));
                        }
                        catch (ex) {
                            alert("Wrong certificate format");
                            return;
                        }
                        //endregion

                        //region Set all "flag variables"
                        certBodyEncoded = "";

                        started = false;
                        waitForEnd = true;
                        //endregion
                    }
                }
            }
            else {
                if (waitForEndLine === true) {
                    if (endLineChars.indexOf(String.fromCharCode(view[i])) === (-1)) {
                        waitForEndLine = false;

                        if (waitForEnd === true) {
                            waitForEnd = false;
                            middleStage = true;
                        }
                        else {
                            if (waitForStart === true) {
                                waitForStart = false;
                                started = true;

                                certBodyEncoded = certBodyEncoded + String.fromCharCode(view[i]);
                            }
                            else
                                middleStage = true;
                        }
                    }
                }
                else {
                    if (middleStage === true) {
                        if (String.fromCharCode(view[i]) === "-") {
                            if ((i === 0) ||
                                ((String.fromCharCode(view[i - 1]) === "\r") ||
                                    (String.fromCharCode(view[i - 1]) === "\n"))) {
                                middleStage = false;
                                waitForStart = true;
                            }
                        }
                    }
                    else {
                        if (waitForStart === true) {
                            if (startChars.indexOf(String.fromCharCode(view[i])) === (-1))
                                waitForEndLine = true;
                        }
                        else {
                            if (waitForEnd === true) {
                                if (endChars.indexOf(String.fromCharCode(view[i])) === (-1))
                                    waitForEndLine = true;
                            }
                        }
                    }
                }
            }
        }
    }
    return verifyCMSSigned(cmsSignedBuffer);
}