import React, { Component } from 'react';
import './App.css';
import { Card, CardContent, FormLabel, Typography } from 'material-ui';
import createCMSsigned from "./createCMSSigned";

let dataBuffer = new ArrayBuffer(0);

class CreateCMSSignedForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            result: ""
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(evt)
    {
        const tempReader = new FileReader();

        const currentFiles = evt.target.files;

        // noinspection AnonymousFunctionJS
        tempReader.onload =
            event =>
            {
                // noinspection JSUnresolvedVariable
                dataBuffer = event.target.result;
                try {
                    createCMSsigned(dataBuffer).then(
                        res => this.setState({
                            result: res
                        }))
                } catch (e) {
                    this.setState({
                        result: e.toString()
                    })
                }
            };

        tempReader.readAsArrayBuffer(currentFiles[0]);
    }

    render() {
        return (
            <form className="center" autoComplete="off" onSubmit={this.handleCreateCMSSigned}>
                <FormLabel style={{textAlign:"left"}} component="legend">Upload File</FormLabel>
                <input
                    id="input-file"
                    type="file"
                    onChange={this.handleChange}
                />
                <Card style={{margin:'30px'}}>
                    <CardContent>
                        <Typography component="p">
                            New CSM signed data + new encoded certificate + PKCS#8 exported private key will be stored here:
                            {this.state.result}
                        </Typography>
                    </CardContent>
                </Card>
            </form>
        );
    }
}

export default CreateCMSSignedForm;